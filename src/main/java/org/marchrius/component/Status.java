package org.marchrius.component;

public class Status {
	private String message;
	private Integer status;
	
	public Status(Integer aStatus, String aMessage) {
		this.status = aStatus;
		this.message = aMessage;
	}
	
	public String getMessage() {
		return this.message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public Integer getStatus() {
		return this.status;
	}
	
	public void setStatus(Integer status) {
		this.status = status;
	}
}
