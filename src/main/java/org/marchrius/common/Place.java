package org.marchrius.common;

import java.sql.Blob;
import java.sql.SQLException;

import org.apache.tomcat.util.codec.binary.Base64;
import org.hibernate.lob.SerializableBlob;


public class Place {
	private Integer id;
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	private String language;
	private String name;
	private Integer resident;
	private byte[] image;
	
	public Place(){}
	
	public Place(Integer aId, String aName, String aLanguage, Integer aResident, byte[] aImage) {
		this.id = aId;
		this.name = aName;
		this.language = aLanguage;
		this.image = aImage;
		this.resident = aResident;
	}
	
	public Integer getResident() {
		return resident;
	}
	
	public void setResident(Integer resident) {
		this.resident = resident;
	}
	
	public byte[] getImage() {
		return image;
	}
	
/*	public void setImage(byte[] image) {
		this.image = image;
	}
	
	public void setImage(String image) {
		this.image = Base64.decodeBase64(image);
	}
	
	public void setImage(SerializableBlob image) {
		try {
			this.image = image.getBytes(1, (int) image.length());
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}*/
	
	public void setImage(Object image) {
		if ( image.getClass() == String.class )
			this.image = Base64.decodeBase64((String)image);
		else if ( image.getClass() == SerializableBlob.class )
			try {
				this.image = ((SerializableBlob)image).getBytes(1,(int) ((SerializableBlob)image).length());
			} catch (SQLException e) {
				e.printStackTrace();
			}
		else if ( image.getClass() == byte.class )
			this.image = (byte[]) image;
		else if ( image.getClass() == Blob.class)
			try {
				this.image = ((Blob)image).getBytes(1, (int) ((Blob)image).length());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		else
			this.image = null;
	}
	
	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public static boolean equals(Place a, Place b) {
		return (a.id==b.id && a.name.equals(b.name) && a.language.equals(b.language) && a.image.equals(b.image));
	}
}
