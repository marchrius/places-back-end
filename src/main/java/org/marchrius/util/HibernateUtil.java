package org.marchrius.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
 
// Questa classe gestisce la connessione al database
public class HibernateUtil {
 
    private static final SessionFactory sessionFactory = buildSessionFactory();
    private static final Session session = getSession();
    
    /*
    private static final String DBUsername = System.getenv("OPENSHIFT_MYSQL_DB_USERNAME");
    private static final String DBPassword = System.getenv("OPENSHIFT_MYSQL_DB_PASSWORD");
    private static final String DBHost = System.getenv("OPENSHIFT_MYSQL_DB_HOST");
    private static final String DBPort = System.getenv("OPENSHIFT_MYSQL_DB_PORT");
    private static final String DBName = System.getenv("progetto_luna");
    private static final String DBURL = System.getenv("OPENSHIFT_MYSQL_DB_URL");
    */
    
    private static SessionFactory buildSessionFactory() {
        try {
        	// Configuro hibernate per la lettura del database. In questo caso ho uno spazio per il webservice + database mysql
        	// Per effettuare richieste da qualsiasi rete.
        	// Se si vuole una connessione in locale basta usare il file di configurazione "hibernate.cfg.xml" come segue:
        	Configuration conf = new Configuration().configure("hibernate.cfg.xml");
        	return conf.buildSessionFactory();
        	
        	/*Configuration conf = new Configuration()
        			.setProperty("hibernate.bytecode.use_reflection_optimizer", "false")
        			.setProperty("hibernate.connection.driver_class", "com.mysql.jdbc.Driver")
        			.setProperty("hibernate.connection.username", "adminzgPfIsA")
        			.setProperty("hibernate.connection.password", "qr_56KEXWNQ9")
        			.setProperty("hibernate.connection.url", "jdbc:mysql://localhost:3307/progetto_luna")
        			.setProperty("hibernate.dialect", "org.hibernate.dialect.MySQLDialect")
        			.setProperty("show_sql", "false")
        			.addResource("/org/marchrius/common/Place.hbm.xml");
        	return conf.buildSessionFactory();*/
        }
        catch (Throwable ex) {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }
 
    public static SessionFactory getSessionFactory() {
        return sessionFactory;
    }
    
    public static Session getSession() {
    	return (session!=null) ? session : sessionFactory.openSession();
    }
 
    public static void close() {
    	getSessionFactory().close();
    }
 
}