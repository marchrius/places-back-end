package org.marchrius;

import java.util.ArrayList;

import javax.servlet.http.HttpServletResponse;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.marchrius.common.Place;
import org.marchrius.component.Status;
import org.marchrius.util.HibernateUtil;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@SuppressWarnings("unchecked")


@RestController
@RequestMapping("/")
public class PlaceController {
	
	public PlaceController() {
	}
	
	//	Importante: permessi per il client di leggere la pagina
	@ModelAttribute
	public void setVaryResponseHeader(HttpServletResponse response) {
	    response.setHeader("Access-Control-Allow-Origin", "*");
	}    
	
	// Funzione che restituisce un "ArrayList" di "Place" completo di luoghi presenti nel database.	
	@RequestMapping("/places")
	public ResponseEntity<ArrayList<Place>> getPlacesDetail() {
        Session session = HibernateUtil.getSession();
        session.beginTransaction();
        
        ArrayList<Place> places = new ArrayList<Place>();
        try {
        	places = (ArrayList<Place>) session.createQuery("from Place").list();
		} catch (HibernateException e) {
	        e.printStackTrace();
	        return new ResponseEntity<ArrayList<Place>>(places, HttpStatus.INTERNAL_SERVER_ERROR);
	    }
		return new ResponseEntity<ArrayList<Place>>(places, HttpStatus.OK);
	}
	
	// Funzione che restituisce un solo luogo selezionato tramite il campo "name", chiave primaria della tabella
	@RequestMapping(value = "/{name}", method = RequestMethod.GET )
	public ResponseEntity<Place> getPlaceDetail(@PathVariable("name") String name) {
		Session session = HibernateUtil.getSession();
		session.beginTransaction();
		
		Place place = new Place();
		try {
			place = (Place) session.createQuery("from Place p where p.name='" + name + "'").list().get(0);
		} catch (ArrayIndexOutOfBoundsException e) {
			e.printStackTrace();
	        return new ResponseEntity<Place>(place, HttpStatus.INTERNAL_SERVER_ERROR);
		} catch (IndexOutOfBoundsException e) {
			e.printStackTrace();
			return new ResponseEntity<Place>(place, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
 		return new ResponseEntity<Place>(place, HttpStatus.OK);
	}

	// TODO: tramite metodo POST invio i nuovi valori che andranno ad aggiornare la tabella.
	// Andranno a sostituire quelli esistenti e aggiungere quelli non esistenti.
	@RequestMapping(value = "/update", method = RequestMethod.POST )
	public @ResponseBody ResponseEntity<Status> updatePlace(@RequestBody Place place) {
		if (place.getName() == null || place.getImage() == null || place.getLanguage() == null || place.getResident() < 0 || place.getId() < 1)
			return new ResponseEntity<Status>(new Status(0, "Invalid object"), HttpStatus.INTERNAL_SERVER_ERROR);
		
		Session session = HibernateUtil.getSession();
		Transaction transaction = session.beginTransaction();
		
		// Recupero il record presente in tabella per effettuare dei controlli. Es. se è uguale al record
		// che sto per inserire allora non effettuo l'operazione.
		Place oldPlace = (Place) session.get(Place.class, place.getId());
		
		if (oldPlace.equals(place))
			return new ResponseEntity<Status>(new Status(1, "No changes made"), HttpStatus.OK);
		
		try {
			session.update(place);
			transaction.commit();
		} catch (HibernateException e) {
			e.printStackTrace();
			return new ResponseEntity<Status>(new Status(1, "Error while updating record with id " + place.getId() + ". Description: " + e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<Status>(new Status(1, "Place with id " + place.getId() + " updated successfully"), HttpStatus.OK);
	}
}